import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import cv2

from neural_wrappers.utilities import RunningMean
from media_processing_lib.image import imgResize
from media_processing_lib.video import tryReadVideo
from argparse import ArgumentParser
from skvideo.io import FFmpegWriter
from tqdm import trange
from vke.body import vidGetBodyKeypoints, imageDisplayBodyParts

from descriptors import getDescriptorType

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("video1Path")
	parser.add_argument("video2Path")
	parser.add_argument("descriptorType")
	args = parser.parse_args()
	args.video1Path = os.path.abspath(os.path.realpath(args.video1Path))
	args.video2Path = os.path.abspath(os.path.realpath(args.video2Path))
	return args

def vidSaveMatch(video1, video2, kp1, kp2, similarity, outPath="match.mp4"):
	vcodec = 'libvpx-vp9' #'libx264'
	print("Save path %s" % outPath)
	fps = str(video1.fps)
	writer = FFmpegWriter(outPath, inputdict={'-r': fps}, outputdict={'-r': fps, \
		'-bitrate': '-1', '-vcodec': vcodec, '-pix_fmt': 'yuv420p', '-lossless': '1'})

	radius = 5
	thickness = -1
	N = min(len(video1), len(video2)) - 1
	width, height = min(video1.shape[1], video2.shape[1]), min(video1.shape[2], video2.shape[2])
	rm = 0
	# Sort each frame by magnitude of keypoint descriptor. Top 5 get green, rest are red.
	for i in trange(N, desc="[EfficientPose] Writing matching video"):
		frame1 = video1[i]
		frame2 = video2[i]
		annot1 = imageDisplayBodyParts(frame1 * 0 + 255, kp1[i])
		annot2 = imageDisplayBodyParts(frame2 * 0 + 255, kp2[i])
		sim = similarity[i]

		frame1 = imgResize(frame1, width=width, height=height, interpolation="bilinear")
		frame2 = imgResize(frame2, width=width, height=height, interpolation="bilinear")
		annot1 = imgResize(annot1, width=width, height=height, interpolation="bilinear")
		annot2 = imgResize(annot2, width=width, height=height, interpolation="bilinear")

		frame = np.zeros((height * 2 + (height // 10) + (height // 10), width * 2, 3), dtype=np.uint8)
		frame[0 : height, 0 : width] = frame1
		frame[0 : height, width :] = annot1
		frame[height : 2 * height, 0 : width] = frame2
		frame[height : 2 * height, width : ] = annot2

		frameSimilarity = sim.mean()
		frameSimilarity = 0 if frameSimilarity < 0.2 else frameSimilarity
		color = np.uint8(np.array([1 - frameSimilarity, frameSimilarity, 0]) * 255)
		frame[2 * height : (2 * height + height // 10)] = color


		size = height // 400
		thickness = height // 200
		pos = (0, 2 * height + height // 10 + height // 15)
		# rm.update(frameSimilarity)
		rm = ((rm * 9) + frameSimilarity) / 10
		Str = "Frame accuracy: %2.3f%%. Global accuracy: %2.3f%%" % (rm * 100, similarity.mean() * 100)
		frame = cv2.putText(frame, Str, pos, cv2.FONT_HERSHEY_SIMPLEX, size, \
			(255, 255, 255), thickness, cv2.LINE_AA)

		# plt.imshow(frame)
		# plt.show()

		writer.writeFrame(frame)
	print("Matching video written to %s" % outPath)

def main():
	args = getArgs()

	video1 = tryReadVideo(args.video1Path, nFrames=None, vidLib="pims")
	kp1 = vidGetBodyKeypoints(video1, "efficient_pose_iii")["coordinates"]
	d1 = getDescriptorType(args.descriptorType)(video1, kp1)

	video2 = tryReadVideo(args.video2Path, nFrames=None, vidLib="pims")
	kp2 = vidGetBodyKeypoints(video2, "efficient_pose_iii")["coordinates"]
	d2 = getDescriptorType(args.descriptorType)(video2, kp2)

	names = [args.video1Path.split("/")[-1].split(".")[0], args.video2Path.split("/")[-1].split(".")[0]]
	match = d1.matchPerFrame(d2)
	print("Mean Match: %2.3f" % (match.mean() * 100))
	print("Global Similarity: %2.3f" % d1.compareGlobalSimilarity(d2))

	# exit()
	vidSaveMatch(video1, video2, kp1, kp2, match)

if __name__ == "__main__":
	main()