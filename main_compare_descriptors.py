import sys
import os

from media_processing_lib.video import tryReadVideo
from argparse import ArgumentParser
from vke.body import vidGetBodyKeypoints
from descriptors import getDescriptorType

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("video1Path")
	parser.add_argument("video2Path")
	parser.add_argument("descriptorType")
	args = parser.parse_args()
	args.video1Path = os.path.abspath(os.path.realpath(args.video1Path))
	args.video2Path = os.path.abspath(os.path.realpath(args.video2Path))
	return args

def main():
	args = getArgs()

	video1 = tryReadVideo(args.video1Path, nFrames=None, vidLib="pims")
	kp1 = vidGetBodyKeypoints(video1, "efficient_pose_iii")["coordinates"]
	d1 = getDescriptorType(args.descriptorType)(video1, kp1)

	video2 = tryReadVideo(args.video2Path, nFrames=None, vidLib="pims")
	kp2 = vidGetBodyKeypoints(video2, "efficient_pose_iii")["coordinates"]
	d2 = getDescriptorType(args.descriptorType)(video2, kp2)

	names = [args.video1Path.split("/")[-1].split(".")[0], args.video2Path.split("/")[-1].split(".")[0]]
	d1.computeComparisonHeatMap(d2, names)
	similarity = d1.compareGlobalSimilarity(d2)
	print("Similarity: %2.2f" % similarity)

if __name__ == "__main__":
	main()